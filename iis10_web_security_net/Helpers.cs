﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Web.Administration;

namespace iis10_web_security_net
{
    public static class Helpers
    {
        public static ConfigurationElement FindElement(ConfigurationElementCollection collection, string elementTagName, params string[] keyValues)
        {
            foreach (ConfigurationElement element in collection)
            {
                if (String.Equals(element.ElementTagName, elementTagName, StringComparison.OrdinalIgnoreCase))
                {
                    bool matches = true;
                    for (int i = 0; i < keyValues.Length; i += 2)
                    {
                        object o = element.GetAttributeValue(keyValues[i]);
                        string value = null;
                        if (o != null)
                        {
                            value = o.ToString();
                        }
                        if (!String.Equals(value, keyValues[i + 1], StringComparison.OrdinalIgnoreCase))
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static void EditWebHostFile(string HostName, string IpAddress)
        {
            string hostFilePath = @"C:\Windows\system32\drivers\etc\hosts";

            int? ipFound = null;

            string[] lines = File.ReadAllLines(hostFilePath);

            for (int i = 0; i < lines.Count(); i++)
            {
                if (lines[i].Trim().StartsWith(IpAddress))
                {
                    ipFound = i;

                    break;
                }
            }

            Thread.Sleep(1000);

            if (ipFound != null)
            {
                lines[Convert.ToInt32(ipFound)] = IpAddress + " " + HostName;

                File.WriteAllLines(hostFilePath, lines);
            }
            else
            {
                List<string> itemsList = lines.ToList<string>();

                itemsList.Add(IpAddress + " " + HostName);

                string[] newArray = itemsList.ToArray();

                File.WriteAllLines(hostFilePath, newArray);

            }
        }

        public static List<Tuple<List<string>, string, TimeSpan>> OutputCacheFactory()
        {
            List<Tuple<List<string>, string, TimeSpan>> result = new List<Tuple<List<string>, string, TimeSpan>>();

            result.Add(new Tuple<List<string>, string, TimeSpan>(new List<string>() { ".jpg", ".img" }, "CacheForTimePeriod", new TimeSpan(1, 0, 0)));
            result.Add(new Tuple<List<string>, string, TimeSpan>(new List<string>() { ".jpg", ".img" }, "CacheForTimePeriod", new TimeSpan(1, 0, 0)));

            return result;

        }

    }
}
