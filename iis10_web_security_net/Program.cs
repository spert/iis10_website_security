﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace iis10_web_security_net
{
    public static class Settings
    {

        // Apppool
        public static string APPPOOLNAME = "MyWebSite";
        public static bool AUTOSTART = true;
        public static string ALWAYSRUNNING = "AlwaysRunning";
        public static bool ENABLE32BITAPPONWIN64 = false;

        //Website
        public static string WEBSITENAME = "MyWebSite";
        public static string HOSTNAME = "www.mywebsite.ee";
        public static string IPADDRESS = "192.168.0.22";
        public static string PHYSICALPATH = @"C:\inetpub\MyWebSite\";
        public static bool PRELOADENABLED = true;
        public static TimeSpan IDLETIMEOUT = new TimeSpan(0, 0, 0); // https://docs.microsoft.com/en-us/iis/configuration/system.applicationhost/applicationpools/add/processmodel 
        public static TimeSpan PERIODICRESTART = new TimeSpan(1, 0, 0); // https://docs.microsoft.com/en-us/iis/configuration/system.applicationhost/applicationpools/add/recycling/periodicrestart/
        public static bool URLDYANMICCOMPRESSION = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webServer/urlCompression
        public static bool URLSTATICCOMPRESSION = true; // https://docs.microsoft.com/en-us/iis/configuration/system.webServer/urlCompression
        public static string CUSTOMHEADERNAME = "Cache-Control";  //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/httpprotocol/customheaders/
        public static string CUSTOMHEADERVALUE = "max-age=604800, private, public"; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/httpprotocol/customheaders/
        public static bool CLIENTCACHEMAXAGE = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/staticcontent/clientcache
        public static TimeSpan CLIENTCACHEMAXAGETIMESPAN = new TimeSpan(7, 0, 0, 0); //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/staticcontent/clientcache
        public static TimeSpan CLIENTCACHEEXPIRESATTIMESPAN = new TimeSpan(90, 0, 0, 0); // TODO: once in week //https://docs.microsoft.com/en-us/iis/configuration/system.webServer/staticContent/clientCache
        public static bool OUTPUTCACHE = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/staticcontent/
        public static List<Tuple<List<string>, string, TimeSpan>> OUTPUTCACHELIST = Helpers.OutputCacheFactory();

    }


    class Program
    {
        static void Main(string[] args)
        {
            // Start Visual Studio in administration mode

            // Turn Windows features on or off
            // Internet Information Services --> Enable
            // Internet Information Services > World Wide Web Services > Application Development Features > Application Initialization --> Enable 
            // Internet Information Services > World Wide Web Services > Performance Features > Dynamic Content Compression --> Enable
            // Internet Information Services > World Wide Web Services > Performance Features > Static Content Compression --> Enable
            // Internet Information Services > Web Management Tools > IIS Management Scripts and Tools --> Enable 
            // Install ASP.NET Core Module --> https://dotnet.microsoft.com/download/dotnet-core/thank-you/runtime-aspnetcore-3.1.3-windows-hosting-bundle-installer
            // Internet Information Services > World Wide Web Services > Application Development Features --> Check (enable) the features. I checked all but CGI. --> Enable 


            ConfigureApppool();
            ConfigueWebSite();



            ConfigureWebConfig();

            Console.WriteLine("Hello World!");
        }



        public static void ConfigureApppool()
        {

            using (ServerManager serverManager = new ServerManager())
            {
                Microsoft.Web.Administration.Configuration config = serverManager.GetApplicationHostConfiguration();
                Microsoft.Web.Administration.ConfigurationSection applicationPoolsSection = config.GetSection("system.applicationHost/applicationPools");
                Microsoft.Web.Administration.ConfigurationElementCollection applicationPoolsCollection = applicationPoolsSection.GetCollection();

                Microsoft.Web.Administration.ConfigurationElement element = Helpers.FindElement(applicationPoolsCollection, "add", "name", Settings.APPPOOLNAME);

                if (element != null)
                {
                    applicationPoolsCollection.Remove(element);
                }

                Microsoft.Web.Administration.ConfigurationElement addElement = applicationPoolsCollection.CreateElement("add");
                addElement["name"] = Settings.APPPOOLNAME;
                addElement["managedRuntimeVersion"] = "4.0";
                addElement["autoStart"] = Settings.AUTOSTART;
                addElement["startMode"] = Settings.ALWAYSRUNNING;
                addElement["enable32BitAppOnWin64"] = Settings.ENABLE32BITAPPONWIN64;

                Microsoft.Web.Administration.ConfigurationElement processModelElement = addElement.GetChildElement("processModel");
                processModelElement["idleTimeout"] = Settings.IDLETIMEOUT;

                Microsoft.Web.Administration.ConfigurationElement recyclingElement = addElement.GetChildElement("recycling");
                Microsoft.Web.Administration.ConfigurationElement periodicRestartElement = recyclingElement.GetChildElement("periodicRestart");
                Microsoft.Web.Administration.ConfigurationElementCollection scheduleCollection = periodicRestartElement.GetCollection("schedule");
                Microsoft.Web.Administration.ConfigurationElement addElement1 = scheduleCollection.CreateElement("add");
                addElement1["value"] = Settings.PERIODICRESTART;
                scheduleCollection.Add(addElement1);
                applicationPoolsCollection.Add(addElement);


                serverManager.CommitChanges();
            }

        }

        public static void ConfigueWebSite()
        {

            using (ServerManager serverManager = new ServerManager())
            {

                Microsoft.Web.Administration.Configuration config = serverManager.GetApplicationHostConfiguration();
                Microsoft.Web.Administration.ConfigurationSection siteSection = config.GetSection("system.applicationHost/sites");
                Microsoft.Web.Administration.ConfigurationElementCollection sitesCollection = siteSection.GetCollection();

                Microsoft.Web.Administration.ConfigurationElement findElement = Helpers.FindElement(sitesCollection, "site", "name", Settings.WEBSITENAME);

                if (findElement != null)
                {
                    sitesCollection.Remove(findElement);
                }

                Microsoft.Web.Administration.ConfigurationElement siteElement = sitesCollection.CreateElement("site");
                siteElement["name"] = Settings.WEBSITENAME;
                siteElement["serverAutoStart"] = Settings.AUTOSTART;
                siteElement["id"] = sitesCollection.Count + 1;

                Microsoft.Web.Administration.ConfigurationElementCollection bindingsCollection = siteElement.GetCollection("bindings");
                Microsoft.Web.Administration.ConfigurationElement bindingElement = bindingsCollection.CreateElement("binding");
                bindingElement["protocol"] = @"http";

                if (!string.IsNullOrEmpty(Settings.HOSTNAME))
                {
                    bindingElement["bindingInformation"] = @"*:80:" + Settings.HOSTNAME;

                    Helpers.EditWebHostFile(Settings.HOSTNAME, Settings.IPADDRESS);
                }
                else
                {
                    bindingElement["bindingInformation"] = @"*:80:" + Settings.IPADDRESS;
                }

                bindingsCollection.Add(bindingElement);
                sitesCollection.Add(siteElement);

                Microsoft.Web.Administration.ConfigurationElementCollection siteCollection = siteElement.GetCollection();
                Microsoft.Web.Administration.ConfigurationElement applicationElement = siteCollection.CreateElement("application");
                applicationElement["preloadEnabled"] = Settings.PRELOADENABLED;
                applicationElement["path"] = @"/";
                Microsoft.Web.Administration.ConfigurationElementCollection applicationCollection = applicationElement.GetCollection();
                Microsoft.Web.Administration.ConfigurationElement virtualDirectoryElement = applicationCollection.CreateElement("virtualDirectory");
                virtualDirectoryElement["path"] = @"/";
                virtualDirectoryElement["physicalPath"] = Settings.PHYSICALPATH;
                applicationCollection.Add(virtualDirectoryElement);
                siteCollection.Add(applicationElement);

                //ConfigurationSection urlCompressionSection = config.GetSection("system.webServer/urlCompression");
                //urlCompressionSection["doStaticCompression"] = Settings.URLSTATICCOMPRESSION;
                //urlCompressionSection["doDynamicCompression"] = Settings.URLDYANMICCOMPRESSION;

                serverManager.CommitChanges();
            }

        }

        public static void ConfigureWebConfig()
        {

            using (ServerManager serverManager = new ServerManager())
            {
                Microsoft.Web.Administration.Configuration config = serverManager.GetWebConfiguration(Settings.WEBSITENAME);

                Microsoft.Web.Administration.ConfigurationSection appInitSection = config.GetSection("system.webServer/applicationInitialization");

                Microsoft.Web.Administration.ConfigurationElementCollection modulesCollection = appInitSection.GetCollection();

                appInitSection.SetAttributeValue("remapManagedRequestsTo", "loading.htm");
                appInitSection.SetAttributeValue("skipManagedModules", false);

                foreach (var elem in modulesCollection.ToList())
                {
                    foreach (var att in elem.Attributes.ToList())
                    {
                        if (att.Name.Equals("initializationPage"))
                        {
                            modulesCollection.Remove(elem);
                        }
                    }
                }

                Microsoft.Web.Administration.ConfigurationElement addElement = modulesCollection.CreateElement("add");
                addElement["initializationPage"] = @"/default.aspx";
                modulesCollection.Add(addElement);

                Microsoft.Web.Administration.ConfigurationSection urlCompression = config.GetSection("system.webServer/urlCompression");
                urlCompression.SetAttributeValue("doStaticCompression", Settings.URLSTATICCOMPRESSION);
                urlCompression.SetAttributeValue("doDynamicCompression", Settings.URLDYANMICCOMPRESSION);


                Microsoft.Web.Administration.ConfigurationSection httpProtocolSection = config.GetSection("system.webServer/httpProtocol");
                httpProtocolSection.SetAttributeValue("allowKeepAlive", true);

                Microsoft.Web.Administration.ConfigurationElementCollection customHeadersCollection = httpProtocolSection.GetCollection("customHeaders");

                foreach (var elem in customHeadersCollection.ToList())
                {
                    foreach (var att in elem.Attributes.ToList())
                    {
                        if (att.Value.Equals("Cache-Control"))
                        {
                            customHeadersCollection.Remove(elem);
                        }
                    }
                }

                Microsoft.Web.Administration.ConfigurationElement addHTTPElement = customHeadersCollection.CreateElement("add");
                addHTTPElement["name"] = Settings.CUSTOMHEADERNAME;
                addHTTPElement["value"] = Settings.CUSTOMHEADERVALUE;
                customHeadersCollection.Add(addHTTPElement);

                // -------------

                Microsoft.Web.Administration.ConfigurationSection staticContentSection = config.GetSection("system.webServer/staticContent");
                Microsoft.Web.Administration.ConfigurationElement clientCacheElement = staticContentSection.GetChildElement("clientCache");

                if (Settings.CLIENTCACHEMAXAGE)
                {
                    clientCacheElement.SetAttributeValue("cacheControlMode", "UseMaxAge");
                    clientCacheElement.SetAttributeValue("cacheControlMaxAge", Settings.CLIENTCACHEMAXAGETIMESPAN);

                }
                else
                {
                    clientCacheElement.SetAttributeValue("cacheControlMode", "UseExpires");
                    clientCacheElement.SetAttributeValue("httpExpires", DateTime.UtcNow.Add(Settings.CLIENTCACHEMAXAGETIMESPAN));
                }

                if (Settings.OUTPUTCACHE)
                {
                    var outputCacheSection = ConfigurationManager.GetSection("system.web/caching/outputCacheSettings") as OutputCacheSettingsSection; 

                    var outputCacheProfilesCollection = outputCacheSection.OutputCacheProfiles;


                    foreach (var lstItem in Settings.OUTPUTCACHELIST)
                    {
                        foreach (var item in lstItem.Item1)
                        {

                            if (outputCacheProfilesCollection.AllKeys.Contains(item))
                            {
                                outputCacheProfilesCollection.Remove(item);
                            }
                            else
                            {
                                OutputCacheProfile profile = new OutputCacheProfile(".jpg");
                                profile.ElementInformation




                            }
                            //ConfigurationElement profileToAdd = cacheProfilesCollection.CreateElement("add");
                            //profileToAdd["extension"] = item;
                            //profileToAdd["policy"] = lstItem.Item2;
                            //profileToAdd["duration"] = lstItem.Item3;
                            //cacheProfilesCollection.Add(profileToAdd);
                        }
                    }
                }

                serverManager.CommitChanges();

            }

            string loadingPage = string.Concat(Settings.PHYSICALPATH, "loading.htm");

            FileInfo fi = new FileInfo(loadingPage);

            if (fi.Exists)
            {
                fi.Delete();
            }

            using (StreamWriter sw = fi.CreateText())
            {
                sw.WriteLine("<meta http-equiv=" + @"""" + "refresh" + @"""" + " content=" + @"""" + "5" + @"""" + ">");
                sw.WriteLine("<h1>Loading...</h1>");
                sw.WriteLine("<p>Please wait while this page loads...</p>");
            }
        }
    }
}
