﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Xml.Linq;
using System.Xml.XPath;

namespace iis10_website_security
{
    public static class Settings
    {

        // Preconditions
        /*
         Start Visual Studio in administration mode
         Turn Windows features on or off

         Kaks faili mida töödeldakse:
         C:\Windows\System32\inetsrv\config\applicationHost.config


         Internet Information Services > Web Management Tools > IIS Management Console --> Enable 
         Internet Information Services > Web Management Tools > IIS Management Scripts and Tools --> Enable 

         Internet Information Services --> Enable
         Internet Information Services > World Wide Web Services > Application Development Features > Application Initialization --> Enable 
         Internet Information Services > World Wide Web Services > Performance Features > Dynamic Content Compression --> Enable
         Internet Information Services > World Wide Web Services > Performance Features > Static Content Compression --> Enable
         Internet Information Services > World Wide Web Services > Security > IP Security --> Enable
         Internet Information Services > World Wide Web Services > Security > Request filtering --> Enable

         Install ASP.NET Core Module --> https://dotnet.microsoft.com/download/dotnet-core/thank-you/runtime-aspnetcore-3.1.3-windows-hosting-bundle-installer
         Internet Information Services > World Wide Web Services > Application Development Features --> Check (enable) the features. I checked all but CGI. --> Enable 
        */

        // Apppool
        public static string APPPOOLNAME = "MyWebSite";
        public static bool AUTOSTART = true;
        public static string ALWAYSRUNNING = "AlwaysRunning";
        public static bool ENABLE32BITAPPONWIN64 = false;
        public static int MAXCONCURRENTREQUESTPERCPU = 500;
        public static int MAXCONCURRENTTHREADSPERCPU = 2;
        public static int QUEUELENGTH = 25;
        public static int CPULIMIT = 40000;
        public static string CPUACTION = "KillW3wp";
        public static TimeSpan CPURESETINTERVAL = new TimeSpan(0, 5, 0);

        //Website
        public static string WEBSITENAME = "MyWebSite";
        public static string HOSTNAME = "www.mywebsite.ee";
        public static string IPADDRESS = "192.168.0.22";
        public static string PHYSICALPATH = @"C:\inetpub\MyWebSite\"; //slash peab olema lõpus
        public static bool PRELOADENABLED = true;
        public static TimeSpan IDLETIMEOUT = new TimeSpan(0,0,0); // https://docs.microsoft.com/en-us/iis/configuration/system.applicationhost/applicationpools/add/processmodel 
        public static string IDLETIMEOUTACTION = "Suspend";
        public static TimeSpan SHUTDOWNTIMELIMIT = new TimeSpan(0, 0, 30);
        public static TimeSpan STARTUPTIMELIMIT = new TimeSpan(0, 0, 30);
        public static bool PINGENABLED = true;
        public static TimeSpan PINGINTERVAL = new TimeSpan(0, 1, 0);
        public static TimeSpan PINGRESPONSETIME = new TimeSpan(0, 0, 30);

        //RECYCLING
        public static TimeSpan RECYCLINGSCHEDULEINTERVAL = new TimeSpan(0, 0, 0);
        public static TimeSpan RECYCLINGSCHEDULETIME = new TimeSpan(5, 0, 0); // https://docs.microsoft.com/en-us/iis/configuration/system.applicationhost/applicationpools/add/recycling/periodicrestart/
        public static int RECYCLINGMEMORY = 300;
        public static int RECYCLINGPRIVATEMEMORY = 300;
        public static string RECYCLINGLOGEVENTS = "Time, Schedule, Memory";

        public static bool URLDYANMICCOMPRESSION = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webServer/urlCompression
        public static bool URLSTATICCOMPRESSION = true; // https://docs.microsoft.com/en-us/iis/configuration/system.webServer/urlCompression
        
        //HTTP HEADERS
        public static string HEADERCACHECONTROLNAME = "Cache-Control";  //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/httpprotocol/customheaders/
        public static string HEADERCACHECONTROLVALUE = "max-age=604800, private, public"; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/httpprotocol/customheaders/
        public static string HEADERCONTENTSECURITYPOLICYNAME = "Content-Security-Policy";  
        public static string HEADERCONTENTSECURITYPOLICYVALUE = "frame-anchestors 'none' default-src 'self'";
        public static string HEADERSTRICTTRANSPORTSECURTYNAME = "Strict-Transport-Security";
        public static string HEADERSTRICTTRANSPORTSECURTYVALUE = "43200";
        public static string HEADERXCONTENTTYPENAME = "X-Content-Type-Options";
        public static string HEADERXCONTENTTYPEVALUE = "nosniff";
        public static string HEADERXDOWNLOADNAME = "X-Download-Options";
        public static string HEADERXDOWNLOADVALUE = "noopen";
        public static string HEADERXPOWEREDBY = "X-Powered-By";

        //CACHE HTTP HEADER
        public static bool HTTPHEADERMAXAGE = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/staticcontent/clientcache
        public static TimeSpan HTTPHEADERMAXAGETIMESPAN = new TimeSpan(7,0,0,0); //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/staticcontent/clientcache
        public static TimeSpan HTTPHEADEREXPIRESATTIMESPAN = new TimeSpan(90, 0, 0, 0); //https://docs.microsoft.com/en-us/iis/configuration/system.webServer/staticContent/clientCache
        
        //OUTPUT CACHE
        public static bool ENABLEUSEROUTPUTCACHE = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/caching/profiles/add
        public static bool ENABLEKERNELOUTPUTCACHE = true; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/caching/profiles/add
        public static Dictionary<string, Dictionary<string, string>> OUTPUTCACHESETTINGS = Helpers.OutputCacheSettingsFactory();   //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/caching/profiles/add

        //Dynamic IP restrictions - NB! this setting is applied on sever level, does not make any sense to apply on website level 
        public static string DYNIPSECDENYACTION = "NotFound";  // https://docs.microsoft.com/en-us/iis/configuration/system.webserver/security/dynamicipsecurity/
        public static bool DYNIPSECPROXYMODE = false;
        public static bool DYNIPSECLOGIONLYMODE = false;
        public static bool DYNIPSECDENYBYCONCURRENTREQUESTS = true;
        public static int DYNIPSECMAXCONCURRENT = 10;
        public static bool DYNIPSECDENYBYREQUESTRATE = true;
        public static int DYNIPSECMAXREQUESTRATE = 10;
        public static int DYNIPSECMAXREQUESTSINTERVAL = 10;

        //Request filtering
        public static int MAXALLOWEDCONTENTLENGTH = 3000000; //https://docs.microsoft.com/en-us/iis/configuration/system.webserver/security/requestfiltering/
        public static int MAXURL = 4096;
        public static int MAXMAXQUERYSTRING = 20481;
        public static bool ALLOWDOUBLEESCAPING = false;
        public static bool ALLOWHIGHBITCHARACTERS = false;
        public static List<string> URLSEQUENCESDENIED = new List<string>() {@"..", @"./", @"\" , @":", @";:" , @";&"};
        public static List<string> DISALLOWEDWERBS = new List<string>() { @"PROPPATCH", @"MKCOL", @"DELETE", @"PUT", @"COPY", @"MOVE", @"LOCK", @"TRACE", @"UNLOCK", @"OPTIONS", @"SEARCH" };
        public static List<string> DISALLOWEDEXTENSIONS = new List<string> { ".back", ".bak", ".bat", ".bas", ".cer", ".cfg", ".com", ".config", ".dat", ".dll", ".exe", ".fcgi", ".jsp", ".log", ".mdb", ".pfx", ".php", ".php3", ".ps1", ".sql", ".tmp", ".vbe", ".vbs" }; 

    }


    class Program
    {
        static void Main(string[] args)
        {
            ConfigureApppool();
            ConfigueWebSite();
            ConfigureWebConfig();

            //// Set IP restrictions
            Helpers.AddDynamicIpRestrictions();

            Helpers.EncryptConnStrings();
            Helpers.DecryptConnStrings();

            Console.WriteLine("Hello World!");
        }
               


        public static void ConfigureApppool()
        {

            using (ServerManager serverManager = new ServerManager())
            {
                Configuration config = serverManager.GetApplicationHostConfiguration();
                ConfigurationSection applicationPoolsSection = config.GetSection("system.applicationHost/applicationPools");
                ConfigurationElementCollection applicationPoolsCollection = applicationPoolsSection.GetCollection();

                ConfigurationElement element = Helpers.FindElement(applicationPoolsCollection, "add", "name", Settings.APPPOOLNAME);

                if (element != null)
                {
                    applicationPoolsCollection.Remove(element);
                }

                ConfigurationElement addElement = applicationPoolsCollection.CreateElement("add");
                addElement["name"] = Settings.APPPOOLNAME;
                addElement["managedRuntimeVersion"] = "4.0";
                addElement["autoStart"] = Settings.AUTOSTART;
                addElement["startMode"] = Settings.ALWAYSRUNNING;
                addElement["enable32BitAppOnWin64"] = Settings.ENABLE32BITAPPONWIN64;
                addElement["queueLength"] = Settings.QUEUELENGTH;

                ConfigurationElement processModelElement = addElement.GetChildElement("processModel");
                processModelElement["idleTimeout"] = Settings.IDLETIMEOUT;
                processModelElement["idleTimeoutAction"] = Settings.IDLETIMEOUTACTION;

                processModelElement["shutdownTimeLimit"] = Settings.SHUTDOWNTIMELIMIT;
                processModelElement["startupTimeLimit"] = Settings.STARTUPTIMELIMIT;
                processModelElement["pingingEnabled"] = Settings.PINGENABLED;
                processModelElement["pingInterval"] = Settings.PINGINTERVAL;
                processModelElement["pingResponseTime"] = Settings.PINGRESPONSETIME;

                ConfigurationElement recyclingElement = addElement.GetChildElement("recycling");
                recyclingElement["logEventOnRecycle"] = Settings.RECYCLINGLOGEVENTS;

                ConfigurationElement periodicRestartElement = recyclingElement.GetChildElement("periodicRestart");
                periodicRestartElement["memory"] = Settings.RECYCLINGPRIVATEMEMORY;
                periodicRestartElement["privateMemory"] = Settings.RECYCLINGPRIVATEMEMORY;

                if (Settings.RECYCLINGSCHEDULEINTERVAL > new TimeSpan(0, 0, 0))
                {
                    periodicRestartElement["time"] = Settings.RECYCLINGSCHEDULEINTERVAL;
                }
                else
                {
                    ConfigurationElementCollection scheduleCollection = periodicRestartElement.GetCollection("schedule");
                    ConfigurationElement addElement1 = scheduleCollection.CreateElement("add");
                    addElement1["value"] = Settings.RECYCLINGSCHEDULETIME;
                    scheduleCollection.Add(addElement1);
                }

                applicationPoolsCollection.Add(addElement);

                //The CPU element should be located at the end
                ConfigurationElement cpuElement = addElement.GetChildElement("cpu");
                cpuElement["limit"] = Settings.CPULIMIT;
                cpuElement["action"] = Settings.CPUACTION;
                cpuElement["resetInterval"] = Settings.CPURESETINTERVAL;
                applicationPoolsCollection.Add(cpuElement);

                serverManager.CommitChanges();

            }
        }       

        public static void ConfigueWebSite()
        {

            using (ServerManager serverManager = new ServerManager())
            {

                Configuration config = serverManager.GetApplicationHostConfiguration();
                ConfigurationSection siteSection = config.GetSection("system.applicationHost/sites");
                ConfigurationElementCollection sitesCollection = siteSection.GetCollection();

                ConfigurationElement findElement = Helpers.FindElement(sitesCollection, "site", "name", Settings.WEBSITENAME);

                if (findElement != null)
                {
                    sitesCollection.Remove(findElement);
                }

                ConfigurationElement siteElement = sitesCollection.CreateElement("site");
                siteElement["name"] = Settings.WEBSITENAME;
                siteElement["serverAutoStart"] = Settings.AUTOSTART;
                siteElement["id"] = sitesCollection.Count + 1;

                ConfigurationElementCollection bindingsCollection = siteElement.GetCollection("bindings");
                ConfigurationElement bindingElement = bindingsCollection.CreateElement("binding");
                bindingElement["protocol"] = @"http";

                if (!string.IsNullOrEmpty(Settings.HOSTNAME))
                {
                    bindingElement["bindingInformation"] = @"*:80:" + Settings.HOSTNAME;

                    Helpers.EditWebHostFile(Settings.HOSTNAME, Settings.IPADDRESS);
                }
                else
                {
                    bindingElement["bindingInformation"] = @"*:80:" + Settings.IPADDRESS;
                }

                bindingsCollection.Add(bindingElement);
                sitesCollection.Add(siteElement);

                ConfigurationElementCollection siteCollection = siteElement.GetCollection();
                ConfigurationElement applicationElement = siteCollection.CreateElement("application");
                applicationElement["preloadEnabled"] = Settings.PRELOADENABLED;
                applicationElement["applicationPool"] = Settings.APPPOOLNAME;
                applicationElement["path"] = @"/";
                ConfigurationElementCollection applicationCollection = applicationElement.GetCollection();
                ConfigurationElement virtualDirectoryElement = applicationCollection.CreateElement("virtualDirectory");
                virtualDirectoryElement["path"] = @"/";
                virtualDirectoryElement["physicalPath"] = Settings.PHYSICALPATH;
                applicationCollection.Add(virtualDirectoryElement);
                siteCollection.Add(applicationElement);


                //ConfigurationSection urlCompressionSection = config.GetSection("system.webServer/urlCompression");
                //urlCompressionSection["doStaticCompression"] = Settings.URLSTATICCOMPRESSION;
                //urlCompressionSection["doDynamicCompression"] = Settings.URLDYANMICCOMPRESSION;



                serverManager.CommitChanges();
            }         

        }

        public static void ConfigureWebConfig()
        {               

            using (ServerManager serverManager = new ServerManager())
            {
                Configuration config = serverManager.GetWebConfiguration(Settings.WEBSITENAME);

                ConfigurationSection appInitSection = config.GetSection("system.webServer/applicationInitialization");

                ConfigurationElementCollection modulesCollection = appInitSection.GetCollection();

                appInitSection.SetAttributeValue("remapManagedRequestsTo", "loading.htm");
                appInitSection.SetAttributeValue("skipManagedModules", false);

                foreach (var elem in modulesCollection.ToList())
                {
                    foreach (var att in elem.Attributes.ToList())
                    {
                        if (att.Name.Equals("initializationPage") )
                        {
                            modulesCollection.Remove(elem);
                        }
                    }
                }

                ConfigurationElement addElement = modulesCollection.CreateElement("add");
                addElement["initializationPage"] = @"/default.aspx";
                modulesCollection.Add(addElement);

                ConfigurationSection urlCompression = config.GetSection("system.webServer/urlCompression");
                urlCompression.SetAttributeValue("doStaticCompression", Settings.URLSTATICCOMPRESSION);
                urlCompression.SetAttributeValue("doDynamicCompression", Settings.URLDYANMICCOMPRESSION);


                ConfigurationSection httpProtocolSection = config.GetSection("system.webServer/httpProtocol");
                httpProtocolSection.SetAttributeValue("allowKeepAlive", true);

                ConfigurationElementCollection customHeadersCollection = httpProtocolSection.GetCollection("customHeaders");
                   
                foreach (var elem in customHeadersCollection.ToList())
                {
                    foreach (var att in elem.Attributes.ToList())
                    {
                        if (att.Value.Equals(Settings.HEADERCACHECONTROLNAME) || 
                            att.Value.Equals(Settings.HEADERCONTENTSECURITYPOLICYNAME) || 
                            att.Value.Equals(Settings.HEADERSTRICTTRANSPORTSECURTYNAME) || 
                            att.Value.Equals(Settings.HEADERXCONTENTTYPENAME) ||
                            att.Value.Equals(Settings.HEADERXDOWNLOADNAME) ||
                            att.Value.Equals(Settings.HEADERXPOWEREDBY))
                        {
                            customHeadersCollection.Remove(elem);
                        }
                    }
                }

                ConfigurationElement cacheControlElement = customHeadersCollection.CreateElement("add");
                cacheControlElement["name"] = Settings.HEADERCACHECONTROLNAME;
                cacheControlElement["value"] = Settings.HEADERCACHECONTROLVALUE;
                customHeadersCollection.Add(cacheControlElement);

                ConfigurationElement contentControlElement = customHeadersCollection.CreateElement("add");
                contentControlElement["name"] = Settings.HEADERCONTENTSECURITYPOLICYNAME;
                contentControlElement["value"] = Settings.HEADERCONTENTSECURITYPOLICYVALUE;
                customHeadersCollection.Add(contentControlElement);

                ConfigurationElement strictTransportSecurityElement = customHeadersCollection.CreateElement("add");
                strictTransportSecurityElement["name"] = Settings.HEADERSTRICTTRANSPORTSECURTYNAME;
                strictTransportSecurityElement["value"] = Settings.HEADERSTRICTTRANSPORTSECURTYVALUE;
                customHeadersCollection.Add(strictTransportSecurityElement);

                ConfigurationElement xcontentTypeElement = customHeadersCollection.CreateElement("add");
                xcontentTypeElement["name"] = Settings.HEADERXCONTENTTYPENAME;
                xcontentTypeElement["value"] = Settings.HEADERXCONTENTTYPEVALUE;
                customHeadersCollection.Add(xcontentTypeElement);

                ConfigurationElement xdownloadElement = customHeadersCollection.CreateElement("add");
                xdownloadElement["name"] = Settings.HEADERXDOWNLOADNAME;
                xdownloadElement["value"] = Settings.HEADERXDOWNLOADVALUE;
                customHeadersCollection.Add(xdownloadElement);

                // -------------

                ConfigurationSection staticContentSection = config.GetSection("system.webServer/staticContent");
                ConfigurationElement clientCacheElement = staticContentSection.GetChildElement("clientCache");

                if (Settings.HTTPHEADERMAXAGE)
                {
                    clientCacheElement.SetAttributeValue("cacheControlMode", "UseMaxAge");
                    clientCacheElement.SetAttributeValue("cacheControlMaxAge", Settings.HTTPHEADERMAXAGETIMESPAN);

                }
                else
                {
                    clientCacheElement.SetAttributeValue("cacheControlMode", "UseExpires");
                    clientCacheElement.SetAttributeValue("httpExpires", DateTime.UtcNow.Add(Settings.HTTPHEADERMAXAGETIMESPAN));
                }



                ConfigurationSection outputCachingSection = config.GetSection("system.webServer/caching");
                outputCachingSection.SetAttributeValue("enabled", Settings.ENABLEUSEROUTPUTCACHE);
                outputCachingSection.SetAttributeValue("enableKernelCache", Settings.ENABLEKERNELOUTPUTCACHE);

                if (Settings.ENABLEUSEROUTPUTCACHE || Settings.ENABLEKERNELOUTPUTCACHE)
                {
                    ConfigurationElementCollection profilesCollection = outputCachingSection.GetCollection("profiles");

                    foreach (var fileExtension in Settings.OUTPUTCACHESETTINGS)
                    {
                        ConfigurationElement findElement = Helpers.FindElement(profilesCollection, "add", "extension", fileExtension.Key);

                        if (findElement != null)
                        {
                            profilesCollection.Remove(findElement);
                        }

                        ConfigurationElement profileToAdd = profilesCollection.CreateElement("add");
                        profileToAdd["extension"] = fileExtension.Key;

                        foreach (var profileItem in fileExtension.Value)
                        {
                            profileToAdd[profileItem.Key] = profileItem.Value;
                        }

                        profilesCollection.Add(profileToAdd);

                    }

                }

                ConfigurationSection requestFilteringSection = config.GetSection("system.webServer/security/requestFiltering");
                requestFilteringSection["allowDoubleEscaping"] = Settings.ALLOWDOUBLEESCAPING;
                requestFilteringSection["allowHighBitCharacters"] = Settings.ALLOWHIGHBITCHARACTERS;

                ConfigurationElement requestLimitElement = requestFilteringSection.GetChildElement("requestLimits");
                requestLimitElement["maxAllowedContentLength"] = Settings.MAXALLOWEDCONTENTLENGTH;
                requestLimitElement["maxUrl"] = Settings.MAXURL;
                requestLimitElement["maxQueryString"] = Settings.MAXMAXQUERYSTRING;


                ConfigurationElementCollection disallowedExtensionsCollection = requestFilteringSection.GetCollection("fileExtensions");
                disallowedExtensionsCollection["allowUnlisted"] = true;

                foreach (var ext in Settings.DISALLOWEDEXTENSIONS)
                {
                    ConfigurationElement reqExtElement = disallowedExtensionsCollection.CreateElement("add");
                    reqExtElement["fileExtension"] = ext;
                    reqExtElement["allowed"] = false;
                    
                    if (!disallowedExtensionsCollection.Any(x => x["fileExtension"].Equals(reqExtElement["fileExtension"])))
                    {
                        disallowedExtensionsCollection.Add(reqExtElement);
                    }
                }

                ConfigurationElementCollection denyVerbsCollection = requestFilteringSection.GetCollection("verbs");
                denyVerbsCollection["allowUnlisted"] = true;

                foreach (var verb in Settings.DISALLOWEDWERBS)
                {
                    ConfigurationElement reqFiltElement = denyVerbsCollection.CreateElement("add");
                    reqFiltElement["verb"] = verb;
                    reqFiltElement["allowed"] = false;

                    if (!denyVerbsCollection.Any(x => x["verb"].Equals(reqFiltElement["verb"])))
                    {
                        denyVerbsCollection.Add(reqFiltElement);
                    }                    
                }

                ConfigurationElementCollection denyUrlSequencesCollection = requestFilteringSection.GetCollection("denyUrlSequences");

                foreach (var seq in Settings.URLSEQUENCESDENIED)
                {
                    ConfigurationElement reqUrlElement = denyUrlSequencesCollection.CreateElement("add");
                    reqUrlElement["sequence"] = seq;

                    if (!denyUrlSequencesCollection.Any(x => x["sequence"].Equals(reqUrlElement["sequence"])))
                    {
                        denyUrlSequencesCollection.Add(reqUrlElement);
                    }
                }                

                serverManager.CommitChanges();
                
            }

            string loadingPage = string.Concat(Settings.PHYSICALPATH, "loading.htm");

            FileInfo fi = new FileInfo(loadingPage);

            if (fi.Exists)
            {
                fi.Delete();
            }

            using (StreamWriter sw = fi.CreateText())
            {
                sw.WriteLine("<meta http-equiv=" + @"""" + "refresh" + @""""  + " content=" + @"""" + "5" + @"""" + ">");
                sw.WriteLine("<h1>Loading...</h1>");
                sw.WriteLine("<p>Please wait while this page loads...</p>");
            }
        }
    }
}


//https://docs.microsoft.com/en-us/iis/configuration/system.webserver/applicationinitialization/


//config.RemoveLocationPath(Settings.WEBSITENAME);

//ConfigurationSection dynamicIpSecuritySection = config.GetSection("system.webServer/security/dynamicIpSecurity");

//dynamicIpSecuritySection["denyAction"] = Settings.DYNIPSECDENYACTION;
//dynamicIpSecuritySection["enableProxyMode"] = Settings.DYNIPSECPROXYMODE;
//dynamicIpSecuritySection["enableLoggingOnlyMode"] = Settings.DYNIPSECLOGIONLYMODE;

//ConfigurationElement denyByConcurrentRequestsElement = dynamicIpSecuritySection.GetChildElement("denyByConcurrentRequests");
//denyByConcurrentRequestsElement["enabled"] = Settings.DYNIPSECDENYBYCONCURRENTREQUESTS;
//denyByConcurrentRequestsElement["maxConcurrentRequests"] = Settings.DYNIPSECMAXCONCURRENT;

//ConfigurationElement denyByRequestRateElement = dynamicIpSecuritySection.GetChildElement("denyByRequestRate");
//denyByRequestRateElement["enabled"] = Settings.DYNIPSECDENYBYREQUESTRATE;
//denyByRequestRateElement["maxRequests"] = Settings.DYNIPSECMAXREQUESTRATE;
//denyByRequestRateElement["requestIntervalInMilliseconds"] = Settings.DYNIPSECMAXREQUESTSINTERVAL;