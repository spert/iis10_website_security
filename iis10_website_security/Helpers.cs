﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace iis10_website_security
{
    public static class Helpers
    {
        public static ConfigurationElement FindElement(ConfigurationElementCollection collection, string elementTagName, params string[] keyValues)
        {
            foreach (ConfigurationElement element in collection)
            {
                if (String.Equals(element.ElementTagName, elementTagName, StringComparison.OrdinalIgnoreCase))
                {
                    bool matches = true;
                    for (int i = 0; i < keyValues.Length; i += 2)
                    {
                        object o = element.GetAttributeValue(keyValues[i]);
                        string value = null;
                        if (o != null)
                        {
                            value = o.ToString();
                        }
                        if (!String.Equals(value, keyValues[i + 1], StringComparison.OrdinalIgnoreCase))
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static void EditWebHostFile(string HostName, string IpAddress)
        {
            string hostFilePath = @"C:\Windows\system32\drivers\etc\hosts";

            int? ipFound = null;

            string[] lines = File.ReadAllLines(hostFilePath);

            for (int i = 0; i < lines.Count(); i++)
            {
                if (lines[i].Trim().StartsWith(IpAddress))
                {
                    ipFound = i;

                    break;
                }
            }

            Thread.Sleep(1000);

            if (ipFound != null)
            {
                lines[Convert.ToInt32(ipFound)] = IpAddress + " " + HostName;

                File.WriteAllLines(hostFilePath, lines);
            }
            else
            {
                List<string> itemsList = lines.ToList<string>();

                itemsList.Add(IpAddress + " " + HostName);

                string[] newArray = itemsList.ToArray();

                File.WriteAllLines(hostFilePath, newArray);

            }
        }

        public static Dictionary<string, Dictionary<string,string>> OutputCacheSettingsFactory()
        {
            Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();

            Dictionary<string, string> set1 = new Dictionary<string, string>();
            set1.Add("location", "Any");
            set1.Add("kernelCachePolicy", "CacheUntilChange");
            set1.Add("policy", "CacheUntilChange");

            result.Add(".jpg",set1);
            result.Add(".css",set1);
            result.Add(".js", set1);

            return result;

        }

        public static void AddDynamicIpRestrictions()
        {

            string applicationHosConfig = @"C:\Windows\System32\inetsrv\config\applicationHost.config";

            if (!File.Exists(applicationHosConfig))
               return;

            XDocument xmlDoc = XDocument.Load(applicationHosConfig);

            var findElements = xmlDoc.Descendants("location").Where(x => x.Attribute("path").Value.Equals(Settings.WEBSITENAME)).ToList();

            foreach (var element in findElements)
            {
                element.Remove();             
            }

            var addElement = xmlDoc.Element("configuration");

            addElement.Add(
                
                new XElement("location", new XAttribute("path", Settings.WEBSITENAME), 
                        new XElement("system.webServer",  
                             new XElement("security", 
                                    new XElement("dynamicIpSecurity", new XAttribute("enableLoggingOnlyMode", Settings.DYNIPSECLOGIONLYMODE),
                                           new XElement("denyByConcurrentRequests", new XAttribute("enabled", Settings.DYNIPSECDENYBYCONCURRENTREQUESTS), new XAttribute("maxConcurrentRequests", Settings.DYNIPSECMAXCONCURRENT)),
                                           new XElement("denyByRequestRate", new XAttribute("enabled", Settings.DYNIPSECDENYBYREQUESTRATE), new XAttribute("maxRequests", Settings.DYNIPSECMAXREQUESTRATE), new XAttribute("requestIntervalInMilliseconds", Settings.DYNIPSECMAXREQUESTSINTERVAL))                                            
                                    )
                              )
                        )
                 )
             );


            xmlDoc.Save(applicationHosConfig);

        }

        public static void EncryptConnStrings()
        {
            string processOutput; int exitCode;

            string exeName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe";

            if (8 == IntPtr.Size || (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
                exeName = @"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe";

            string arguments = $"-pef \"connectionStrings\" \"" + Settings.PHYSICALPATH.Trim('\\') + "\"";

            using (Process process = new Process())
            {
                process.EnableRaisingEvents = true;
                process.StartInfo = new ProcessStartInfo
                {
                    FileName = exeName,
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                };

                process.Start();

                processOutput = process.StandardOutput.ReadToEnd();

                bool exited = process.WaitForExit(1000);

                if (exited)
                {
                    exitCode = process.ExitCode;
                }

            }
        }

        public static void DecryptConnStrings()
        {
            string processOutput; int exitCode;

            string exeName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe";

            if (8 == IntPtr.Size || (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
                exeName = @"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe";

            string arguments = $"-pdf \"connectionStrings\" \"" + Settings.PHYSICALPATH.Trim('\\') + "\"";

            using (Process process = new Process())
            {
                process.EnableRaisingEvents = true;
                process.StartInfo = new ProcessStartInfo
                {
                    FileName = exeName,
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                };

                process.Start();

                processOutput = process.StandardOutput.ReadToEnd();

                bool exited = process.WaitForExit(1000);

                if (exited)
                {
                    exitCode = process.ExitCode;
                }

            }
        }




    }
}
